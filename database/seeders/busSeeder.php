<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class busSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('bus')->insert([
            'plat_number' => 'AD0292019EP',
            'brand' => 'mercedes',
            'seat' => 6,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        \DB::table('bus')->insert([
            'plat_number' => 'AD02920439AF',
            'brand' => 'fuso',
            'seat' => 6,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        \DB::table('bus')->insert([
            'plat_number' => 'AE2939302PD',
            'brand' => 'scania',
            'seat' => 4,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
