<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class orderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('orders')->insert([
            'bus_id' => 4,
            'driver_id' => 9,
            'contact_name' => 'Riana Eka Fidriyani',
            'start_rent_date' => now(),
            'total_rent_days' => 2
        ]); 
        \DB::table('orders')->insert([
            'bus_id' => 4,
            'driver_id' => 9,
            'contact_name' => 'Zainal Arifin',
            'start_rent_date' => now(),
            'total_rent_days' => 2
        ]); 
    }
}
