<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;
class driversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drivers')->insert([
            'name' => 'Zainal Arifin',
            'age' => 18,
            'id_number' => 'BMR'. mt_rand(1,200) 
        ]);
        DB::table('drivers')->insert([
            'name' => 'Niko febriyanto',
            'age' => 18,
            'id_number' =>'BMR'. mt_rand(1,200)
        ]);
        DB::table('drivers')->insert([
            'name' => 'Hendra Agil S',
            'age' => 18,
            'id_number' => 'BMR'. mt_rand(1,200)
        ]);
        DB::table('drivers')->insert([
            'name' => 'Tedy Dwi Prayoga',
            'age' => 18,
            'id_number' =>'BMR'. mt_rand(1,200)
        ]);
    }
}
