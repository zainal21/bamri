<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(busSeeder::class);
        // $this->call(driversSeeder::class);
        $this->call(orderSeeder::class);
        // \App\Models\User::factory(10)->create();
    }
}
