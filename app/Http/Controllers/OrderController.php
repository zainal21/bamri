<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Order,Driver, Bus};
use Inertia\Inertia;
use Validator;
use Illuminate\Support\Facades\Redirect;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Order/index', [
            'order' => Order::orderBy('created_at', 'asc')->with(['bus', 'driver'])->get(),
            'driver' => Driver::all(),
            'bus' => Bus::all()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);
        return Redirect::route('order.index');
    }
}
