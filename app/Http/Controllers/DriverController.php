<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Driver;
use Inertia\Inertia;
use Validator;
use Illuminate\Support\Facades\Redirect;
class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Driver/index', ['driver' => Driver::orderBy('id_number', 'asc')->get()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name',
            'age'
        ]);
        $data = $request->all();
        $data['id_number'] =  'BMR'. mt_rand(1,200);
        Driver::create($data);
        return Redirect::route('driver.index');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name',
            'age'
        ]);
        Driver::where('id', $id)->update([
            'name' => $request->name,
            'age' => $request->age
        ]);
        return Redirect::route('driver.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Driver::destroy($id);
        return Redirect::route('driver.index');
    }
}
