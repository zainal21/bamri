<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Bus;
use Validator;
use Illuminate\Support\Facades\Redirect;
class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Bus/index', ['bus' => Bus::all()]);
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'plat_number' => ['required'],
            'seat' => ['required'],
            'brand' => ['required'],
        ])->validate();
        Bus::create($request->all());
        return Redirect::route('bus.index');
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'plat_number' => ['required'],
            'seat' => ['required'],
            'brand' => ['required'],
        ])->validate();
        Bus::where('id', $id)->update([
            'plat_number' => $request->plat_number,
            'seat' => $request->seat,
            'brand' => $request->brand,
        ]);
        return Redirect::route('bus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bus::destroy($id);
        return Redirect::route('bus.index');
    }
}
