<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'drivers';

    public function Order()
    {
        return $this->hasMany(Order::class, 'driver_id', 'id');
    }
}
