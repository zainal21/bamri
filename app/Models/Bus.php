<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use HasFactory;
    protected $table = 'bus';
    protected $guarded = [];

    public function Order()
    {
        return $this->hasMany(Order::class, 'bus_id', 'id');
    }
}
